const Post = require('../models/post');
const Comment = require('../models/comment');


module.exports = {
    addNewPost(req, res, next) {

        console.log("==============POST CONTROLLER===================");
        const new_post = req.body;
        Post.create(new_post).
        then((post) => {
                res.status(200).json({
                    message: 'Post has been creating successfully!',
                });
            })
            .catch((err) => {
                res.status(500).json({
                    message: 'something went wrong | internal server error!',
                    err: err
                });
            })
    },
    addNewComment(req, res, next) {
        console.log("==============COMMENT CONTROLLER===================");

        const post_id = req.body.post_id;
        const new_comment = {
            _commented_by: req.body._commented_by,
            content: req.body.content
        }
        // createing new comment
        Comment.create(new_comment)
            .then((comment) => {
                // updating post and adding new comment in post
                Post.findByIdAndUpdate({
                        _id: post_id
                    }, {
                        $push: {
                            comments: comment
                        },
                        "$inc": {
                            "comments_count": 1
                        }
                    }, {
                        new: true
                    })
                    .then((updated_post) => {
                        res.status(200).json({
                            message: "comment has been addned successfully!",
                            post: updated_post
                        })
                    })
                    // post updating error handling
                    .catch((err) => {
                        res.status(500).json({
                            message: "something went wrong | internal server error!",
                            err: err
                        })
                    })
            })
            // comment creation error handling
            .catch((err) => {
                res.status(500).json({
                    message: "something went wrong | internal server error!",
                    err: err
                })
            })




    },
    likePost(req, res, next) {
        console.log("==============POST LIKE CONTROLLER===================");

        const post_id = req.body.post_id;
        const liked_by = req.body.liked_by;

        Post.findByIdAndUpdate({
                _id: post_id
            }, {

                $push: {
                    liked_by: liked_by
                },
                "$inc": {
                    "likes_count": 1
                }
            })
            .then((post) => {

                res.status(200).json({
                    message: 'Post has been liked successfully!',
                });

            }).catch((err) => {
                res.status(500).json({
                    message: 'soemthing went wrong | internal server error!',
                });

            })

    },
    dislikePost(req, res, next) {
        console.log("==============POST LIKE CONTROLLER===================");

        const post_id = req.body.post_id;
        const liked_by = req.body.liked_by;

        Post.findByIdAndUpdate({
                _id: post_id
            }, {
                $set: {
                    $pop: {
                        liked_by: liked_by
                    }
                },
                "$inc": {
                    "likes_count": -1
                }
            })
            .then((post) => {

                res.status(200).json({
                    message: 'Post has been liked successfully!',
                });

            }).catch((err) => {
                res.status(500).json({
                    message: 'soemthing went wrong | internal server error!',
                });

            })
    },
    getPosts(req, res, next) {

        console.log("=============CONTROLLER GET POSTS============");

        const page = req.params.page;
        Post.find({})
            .skip(page * 10)
            .limit(10)
            .populate("_posted_by", "firstName  lastName userName")
            .populate({
                path: 'comments',
                select: "content",
                populate: {
                    path: '_commented_by',
                    select: 'firstName lastName userName'
                }
            })
            /* .populate("comments._commented_by", "first_name") */
            .then((posts) => {
                res.status(200).json({
                    message: "Post are found successfully!",
                    posts: posts
                });
            })
            .catch((err) => {
                res.status(500).json({
                    message: "somethign went wrong  | Internal server error!",
                    err: err
                });
            })
    }

}