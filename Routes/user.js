let express = require('express');
let router = express.Router();
let controllerUser = require('../controllers/ControllerUser');
let controllerPost = require('../controllers/ControllerPost');
let file_handler = require('../middleswares/file_upload_handler');


let auth_middleware = require('../middleswares/atuh');


// Auth apis
router.post('/signin', controllerUser.signin);
router.post('/signup', controllerUser.signup);
router.post('/signout', auth_middleware.veryfiyJwtToken, auth_middleware.isLogin, controllerUser.signout);
router.post('/checkUserName', controllerUser.checkUserName);


// User apis
router.get('/:page', auth_middleware.veryfiyJwtToken, auth_middleware.isLogin, controllerUser.getUsers);

// post routes
router.post('/post', auth_middleware.veryfiyJwtToken, auth_middleware.isLogin, file_handler.fileHander, controllerPost.addNewPost);
router.post('/comment', auth_middleware.veryfiyJwtToken, auth_middleware.isLogin, controllerPost.addNewComment);
router.post('/likePost', auth_middleware.veryfiyJwtToken, auth_middleware.isLogin, controllerPost.likePost);
router.post('/dislikePost', auth_middleware.veryfiyJwtToken, auth_middleware.isLogin, controllerPost.dislikePost);
router.get('/post/:page', auth_middleware.veryfiyJwtToken, auth_middleware.isLogin, controllerPost.getPosts);



module.exports = router;