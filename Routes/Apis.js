const express = require('express');
const router = express.Router();



router.get('/', (req, res, next) => {
    console.log("==========Get Route============");
    res.json({
        Message: "Get type request"
    });
})
router.post('/', (req, res, next) => {
    console.log("==========Post Route============");
    res.status(500).json({
        Message: "Post type request"
    });

})
router.delete('/', (req, res, next) => {
    console.log("==========Delete Route============");
    res.json({
        Message: "Delete type request"
    });
})
router.put('/', (req, res, next) => {
    console.log("==========Update Route============");
    res.json({
        Message: "Update type request"
    });

})


module.exports = router;