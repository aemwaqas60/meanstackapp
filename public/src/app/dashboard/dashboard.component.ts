import { Component, OnInit } from '@angular/core';
import { PostService } from '../Services/post.service';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  posts;
  BASE_IMAGE_URL = environment.BASE_IMAGE_URL;
  constructor(private _postService: PostService) { }

  ngOnInit() {
    this.loadPosts();
  }


  loadPosts() {
    this._postService.getPosts()
      .subscribe((res) => {

        this.posts = res['posts'];

        this.posts.map(post => {
          post.files[0] = this.BASE_IMAGE_URL + `${post.files[0]}`;
        });

        console.log('after mapping posts: ', this.posts);


        console.log(this.posts);

      }, (err) => {

        console.log(err);
      });
  }

}
