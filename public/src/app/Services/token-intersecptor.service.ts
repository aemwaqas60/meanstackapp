import { Injectable, Injector } from '@angular/core';
import { HttpInterceptor } from '../../../node_modules/@angular/common/http';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class TokenIntersecptorService implements HttpInterceptor {

  constructor(private _intjector: Injector) { }

  intercept(req, next) {
    const authService = this._intjector.get(AuthService);

    const tokenizedReuest = req.clone({
      setHeaders: {
        Authorization: `Bearer ${authService.getToken()}`
      }
    });
    return next.handle(tokenizedReuest);
  }
}
