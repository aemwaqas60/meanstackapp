import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { AuthService } from '../../Services/auth.service';
import { InputValidtors } from '../../Validators/inputValidator';


import { Router } from '../../../../node_modules/@angular/router';
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {


  signupForm: FormGroup;
  passwordFormGroup: FormGroup;

  user = {
    firstName: '',
    lastName: '',
    email: '',
    userName: '',
    password: ''
  };

  checkUserName = {
    status: '',
    message: ''
  };

  alert = {
    class: '',
    message: ''
  };


  constructor(private _authService: AuthService, private formBuilder: FormBuilder, private _router: Router) { }
  ngOnInit() {

    this.initializeSignupForm();
  }


  initializeSignupForm() {
    // Reactive forms validation
    this.signupForm = this.formBuilder.group({
      firstName: ['', [Validators.required, InputValidtors.isInputEmpty]],
      lastName: ['', [Validators.required, InputValidtors.isInputEmpty]],
      userName: ['', [Validators.required, InputValidtors.isInputEmpty]],
      email: ['', [Validators.required, Validators.email]],
      passwordFormGroup: this.passwordFormGroup
    },
    );
    this.passwordFormGroup = this.formBuilder.group({
      password: ['', [Validators.required, InputValidtors.isInputEmpty]],
      repeatPassword: ['', [Validators.required, InputValidtors.isInputEmpty]]
    }, {
        validator: InputValidtors.validatePassword.bind(this)
      });
  }
  onSignupFormSubmit() {
    // console.log(this.user);

    console.log(this.signupForm);

    this._authService.signup(this.user)
      .subscribe((res) => {

        this.alert.class = 'alert alert-success';
        this.alert.message = res['message'];

        console.log('Reponse: ', res);

        setTimeout(() => {
          this._router.navigate(['/signin']);
        }, 3000);


      }, (err) => {
        console.log('Sigup Error: ', err);

        this.alert.class = 'alert alert-danger';
        this.alert.message = err.error['message'];

      });
  }
  // checking either username is available or not
  onCheckUserName(event) {
    console.log(event);
    console.log('user name value: ', event.target.value);
    const data = {
      userName: event.target.value
    };

    this._authService.checkUserName(data)
      .subscribe((res) => {

        console.log('Reponse: ', res);

        this.checkUserName = res;

      }, (err) => {

        console.log('user name check err: ', err);

      });


  }

}
