import { Component, OnInit } from '@angular/core';
import { Router } from '../../../../node_modules/@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { InputValidtors } from '../../Validators/inputValidator';
import { AuthService } from '../../Services/auth.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {


  signinForm: FormGroup;
  localUserData;
  token;

  alert = {
    message: '',
    class: ''
  };

  user = {
    email: '',
    password: ''
  };
  constructor(private _router: Router, private _authService: AuthService) { }

  ngOnInit() {


    this.localUserData = localStorage.getItem('user');
    this.token = localStorage.getItem('token');

    console.log('token: ', this.token);
    console.log('user local data: ', this.localUserData);

    this.initSigninForm();
  }

  initSigninForm() {
    this.signinForm = new FormGroup({
      'email': new FormControl(null, [Validators.required, InputValidtors.isInputEmpty]),
      'password': new FormControl(null, [Validators.required, Validators.email, InputValidtors.isInputEmpty]),
    });
  }

  onSigninSubmit() {
    // Es6 santax for console data

    console.log('user signin data:', this.user);


    this._authService.signin(this.user)
      .subscribe((res) => {

        console.log('res: ', res);


        this.alert.message = res.message;
        this.alert.class = 'alert alert-success';

        // storing user data and token in browser localstorage
        localStorage.setItem('user', JSON.stringify(res.user));
        localStorage.setItem('token', res.token);

        setTimeout(() => {

          this._router.navigate(['/dashboard']);
        }, 3000);

      }, (err) => {

        console.log(err);

        this.alert.class = 'alert alert-danger';
        this.alert.message = err.error.message;

      });
  }


}
