const mongoose = require("mongoose");
mongoose.Promise = require("bluebird");


// database url and name

const dbUrl = process.env.DBURL || "mongodb://127.0.0.1:27017/MeanStackApp";

mongoose.connect(dbUrl);
mongoose.connection.on('connected', function () {
    console.log("###Mongoose database has connected successfully!");
});
mongoose.connection.on('error', function () {
    console.log("##Mongoose database connection has occored erorr!");
});
mongoose.connection.on('disconnected', function () {
    console.log("##Mongoose dabase has disconnected");

});
process.on('SIGINT', function () {
    mongoose.connection.close(function () {
        console.log("##Mongoose default connection is disconnected due to application termination");
        process.exit(0);
    });
});