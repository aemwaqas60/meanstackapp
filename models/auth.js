const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const User = require('../models/user');


const AuthSchema = new Schema({

    token: {
        type: String,
        default: null
    },
    is_signin: {
        type: Boolean,
        default: false
    },
    _user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
    }

});


const Auth = mongoose.model('Auth', AuthSchema);
module.exports = Auth;