const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const User = require('./user');


const CommmentsSchema = new Schema({

    content: {
        type: String,
        default: null
    },
    files: [{
        type: String,
        default: null
    }],
    likes_count: {
        type: Number,
        default: 0
    },
    _commented_by: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    created_date: {
        type: Date,
        default: Date.new
    }
});


const Comment = mongoose.model('Comment', CommmentsSchema);
module.exports = Comment;