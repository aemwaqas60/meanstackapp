const bcrypt = require('bcrypt');
module.exports = {

    ecnryptPassword(plainPassword) {
        return new Promise((resolve, reject) => {
            bcrypt.hash(plainPassword, 10, (err, encryptedPassword) => {
                if (err) {
                    reject({
                        message: "Password encryption error occured!",
                        err: err
                    })
                } else {
                    resolve({
                        message: "Password  has encrypted successfully!",
                        encryptedPassword: encryptedPassword
                    })
                }
            })
        })
    },
    dcryptPassword(hashPassword, plainPassword) {
        return new Promise((resolve, reject) => {
            bcrypt.compare(plainPassword, hashPassword, (err, res) => {
                if (err) {
                    reject({
                        message: "Password dcryption error occured!",
                        err: err
                    })
                } else {
                    resolve({
                        message: "Password has dcrypted successfully!",
                        response: res
                    })
                }
            })
        })

    }
}